import os
import rid

from flask import Flask, request, render_template
from wtforms import Form, TextAreaField, validators


class SourceForm(Form):
    source = TextAreaField('Source', [validators.Required()])


app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def source_view():
    rid_app = rid.RegressiveImageryDictionary()
    form = SourceForm(request.form)
    if request.method == 'POST' and form.validate():
        source = request.form['source']
        rid_app.load_dictionary_from_string(rid.DEFAULT_RID_DICTIONARY)
        rid_app.load_exclusion_list_from_string(rid.DEFAULT_RID_EXCLUSION_LIST)
        results = rid_app.analyze(source)
        return rid_app.display_results_html(results, 'RID Analysis')
    return render_template('source.html', form=form)


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 8000))
    app.run(host='0.0.0.0', port=port, use_debugger=True, debug=True, use_reloader=True)
